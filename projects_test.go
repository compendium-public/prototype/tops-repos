package repo

import (
	"fmt"
	"strings"
	"testing"
)

func TestProjectsWithBranches(t *testing.T) {
	ps := Projects{
		&Project{
			ID:      "test",
			Name:    "test",
			Server:  "test",
			Backend: MockBackend{},
		},
	}

	ps.WithBranches()

	if len(ps) != 1 || ps[0].ID != "test" {
		out := []string{fmt.Sprintf("expecting one Project of ID test\nreceived: %s", stringify(ps))}
		for _, _p := range ps {
			out = append(out, stringify(*_p))
		}
		t.Fatalf(strings.Join(out, "\n  "))

	}

}
