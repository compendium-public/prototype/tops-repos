package repo

type (
	RepoServers struct {
		RepoServers []RepoServer
	}
)

func (r *RepoServers) AddServer(name string, repo RepoBackend) {
	r.RepoServers = append(r.RepoServers, RepoServer{
		Name:    name,
		Backend: repo,
	})
}

func (r *RepoServers) Projects() (projects Projects, err error) {
	return r.ProjectsByTeam(nil)
}
func (r *RepoServers) ProjectsByTeam(team *Team) (projects Projects, err error) {
	for _, repo := range r.RepoServers {
		projects = append(projects, repo.ProjectsByTeam(team)...)
	}
	return
}

func (r *RepoServers) Teams() (teams Teams, err error) {
	return r.TeamsByTeam(nil)
}

func (r *RepoServers) TeamsByTeam(team *Team) (teams Teams, err error) {
	for _, repo := range r.RepoServers {
		teams = append(teams, repo.TeamsByTeam(team)...)
	}
	return
}
