module gitlab.com/compendium-public/prototype/tops-repos

go 1.15

require (
	github.com/google/go-github v17.0.0+incompatible
	github.com/sirupsen/logrus v1.8.1
	github.com/xanzy/go-gitlab v0.47.0
	golang.org/x/oauth2 v0.0.0-20210220000619-9bb904979d93
)
