package repo

type (
	RepoBackend interface {
		// ProjectsFromTeam takes any level of team and returns all the projects beneth it
		// nil for top level
		ProjectsByTeam(team *Team) Projects

		// TeamsByTeam takes any level of team and returns all the teams beneth it
		// nil for top level
		TeamsByTeam(team *Team) Teams

		// Projects returns all projects as a collective Projects
		Projects() Projects

		// Teams returns all teams as a collective Teams
		Teams() Teams

		ProjectBranches(id interface{}) []Branch

		// ReferencesByCommit pulls all the references for a given commit ID
		ReferencesByCommit(id string) []Commit
	}
)
