package repo

type (
	Branch struct {
		// Name is the git* Name for the thing, probably unique and probably key-able for the server/repo
		Name    string
		ID      interface{}
		Merged  bool
		Project string
		Commits Commits
		Backend RepoBackend `json:"-"`
	}
)
