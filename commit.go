package repo

type (
	Commit struct {
		// Name is the git* Name for the thing, probably unique and probably key-able for the server/repo
		Name       string
		ID         interface{}
		References []string
		Backend    RepoBackend `json:"-"`
	}
)
