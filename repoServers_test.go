package repo

import (
	"fmt"
	"strings"
	"testing"
)

var rs RepoServers

func TestRepoServers(t *testing.T) {
	rs = RepoServers{
		[]RepoServer{
			{
				Name:    "test",
				Backend: MockBackend{},
			},
		},
	}
}

func TestAddServer(t *testing.T) {
	rs.AddServer("anewone", MockBackend{})
}

func TestRepoServersProjects(t *testing.T) {
	rsProjects, _ := rs.Projects()

	if len(rsProjects) != 2 || rsProjects[0].Server != "test" || rsProjects[1].Server != "anewone" {
		out := []string{fmt.Sprintf("expecting two Projects of Server test and anewone\nreceived: %s", stringify(rsProjects))}
		for _, p := range rsProjects {
			out = append(out, stringify(*p))
		}
		t.Fatalf(strings.Join(out, "\n  "))
	}

}

func TestRepoServersTeams(t *testing.T) {
	rsTeams, _ := rs.Teams()

	if len(rsTeams) != 2 || rsTeams[0].Server != "test" || rsTeams[1].Server != "anewone" {
		out := []string{fmt.Sprintf("expecting twoTeams of Server test and anewone\nreceived: %s", stringify(rsTeams))}
		for _, p := range rsTeams {
			out = append(out, stringify(*p))
		}
		t.Fatalf(strings.Join(out, "\n  "))
	}
}
