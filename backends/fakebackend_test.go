package backends

import (
	"testing"

	repo "gitlab.com/compendium-public/prototype/tops-repos"
)

var b repo.RepoBackend

func TestNewFakeRepoBackend(t *testing.T) {
	b = NewFakeRepoBackend()
	testBackend(b, t)
}

func testBackend(b repo.RepoBackend, t *testing.T) {
	if b == nil {
		t.Fatalf("expecting not nil")
	}

	p := b.Projects()

	if len(p) != 0 {
		t.Fatalf("Expecting no projects, found some... %v", p)
	}

	pb := b.ProjectBranches("noop")

	if len(pb) != 0 {
		t.Fatalf("Expecting no projectbranches, found some... %v", pb)
	}

	g := b.Teams()

	if len(g) != 0 {
		t.Fatalf("Expecting no teams, found some... %v", g)
	}

}
