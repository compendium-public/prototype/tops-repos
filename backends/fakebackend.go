package backends

import repo "gitlab.com/compendium-public/prototype/tops-repos"

type (
	FakeRepoBackend struct {
		repo.RepoBackend
	}
)

func NewFakeRepoBackend() *FakeRepoBackend {
	return &FakeRepoBackend{}
}

func (r *FakeRepoBackend) Projects() (projects repo.Projects) {
	return projects
}

func (r *FakeRepoBackend) ProjectsByTeam(team *repo.Team) (projects repo.Projects) {
	return projects
}

func (r *FakeRepoBackend) ProjectBranches(id interface{}) (branches []string) {
	return branches
}

func (r *FakeRepoBackend) Teams() (teams repo.Teams) {
	return teams
}

func (r *FakeRepoBackend) TeamsByTeam(team *repo.Team) (teams repo.Teams) {
	return teams
}
