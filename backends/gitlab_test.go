package backends

import (
	"fmt"
	"os"
	"testing"

	repo "gitlab.com/compendium-public/prototype/tops-repos"
)

var g repo.RepoBackend

// I really dont understand how to make an interface situation for these things with sub structs like .Organization etc...
// so os.Env ugliness...

func TestGitlab(t *testing.T) {
	gitlabTestToken := os.Getenv("GITLAB_TOKEN_FOR_TESTING")
	gitlabEndpoint := os.Getenv("GGITLAB_ENDPOINT_FOR_TESTING")
	if gitlabEndpoint == "" {
		gitlabEndpoint = "https://gitlab.dev.opsgem.cloud"
	}

	g = NewGitlabRepoBackend(gitlabEndpoint, gitlabTestToken)

	if g == nil {
		t.Fatalf("expecting not nil")
	}

	if gitlabTestToken != "" {
		selected := &repo.Team{FullPath: "test-team-1/data-service"}

		// test for sub teams
		subteams := g.TeamsByTeam(selected)
		for _, _t := range subteams {
			fmt.Println(_t.Name)
		}

		// grab something with a sub team
		projects := g.ProjectsByTeam(selected)
		for _, _p := range projects {
			fmt.Println(_p.Name)
		}

	}
}
