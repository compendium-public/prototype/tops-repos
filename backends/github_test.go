package backends

import (
	"os"
	"testing"

	repo "gitlab.com/compendium-public/prototype/tops-repos"
)

var ghb repo.RepoBackend

// I tried implementing an interface for github to test...  I got close, but it didnt work.
// https://github.com/google/go-github/issues/113
// so for now we hack
// which sucks, but... it is forward...

func TestGitHub(t *testing.T) {
	githubTestToken := os.Getenv("GITHUB_TOKEN_FOR_TESTING")
	githubEndpoint := os.Getenv("GITHUB_ENDPOINT_FOR_TESTING")
	if githubEndpoint == "" {
		githubEndpoint = "https://github.com"
	}
	ghb = NewGithubRepoBackend(githubEndpoint, githubTestToken)

	if githubTestToken != "" {
		testBackend(ghb, t)
	}

	if ghb == nil {
		t.Fatalf("expecting not nil")
	}
}
