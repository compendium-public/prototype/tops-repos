package backends

import (
	"context"

	log "github.com/sirupsen/logrus"

	"github.com/google/go-github/github"
	repo "gitlab.com/compendium-public/prototype/tops-repos"
	"golang.org/x/oauth2"
)

type (
	GithubRepoBackend struct {
		repo.RepoBackend
		url   string
		token string
	}
)

func NewGithubRepoBackend(url, token string) *GithubRepoBackend {
	return &GithubRepoBackend{
		url:   url,
		token: token,
	}
}

func (r *GithubRepoBackend) Projects() (projects repo.Projects) {
	return projects
}

func (r *GithubRepoBackend) ProjectBranches(id interface{}) (branches []repo.Branch) {
	return branches
}
func (r *GithubRepoBackend) Teams() (teams repo.Teams) {
	ctx := context.Background()
	ts := oauth2.StaticTokenSource(
		&oauth2.Token{AccessToken: r.token},
	)
	tc := oauth2.NewClient(ctx, ts)

	client, err := github.NewEnterpriseClient(r.url, r.url, tc)
	if err != nil {
		log.Errorf(err.Error())
	}

	orgs, _, err := client.Organizations.List(context.Background(), "", nil)
	if err != nil {
		log.Errorf(err.Error())
	}

	for _, item := range orgs {
		teams = append(teams, &repo.Team{
			Name:     *item.Name,
			ID:       *item.ID,
			Path:     "",
			FullPath: "",
		})

	}

	return teams
}
