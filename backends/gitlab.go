package backends

import (
	log "github.com/sirupsen/logrus"

	"github.com/xanzy/go-gitlab"
	repo "gitlab.com/compendium-public/prototype/tops-repos"
)

type (
	GitlabRepoBackend struct {
		repo.RepoBackend
		url   string
		token string
	}
)

func NewGitlabRepoBackend(url, token string) *GitlabRepoBackend {
	return &GitlabRepoBackend{
		url:   url,
		token: token,
	}
}

func (r *GitlabRepoBackend) Projects() (projects repo.Projects) {
	return r.ProjectsSearch("")
}

func (r *GitlabRepoBackend) ProjectsSearch(search string) (projects repo.Projects) {
	git, err := gitlab.NewClient(
		r.token,
		gitlab.WithBaseURL(r.url),
	)
	if err != nil {
		log.Errorf(err.Error())
	}

	opt := &gitlab.ListProjectsOptions{
		Archived: gitlab.Bool(false),
		//Membership: gitlab.Bool(true),
		Search:           gitlab.String(search),
		SearchNamespaces: gitlab.Bool(true),
		ListOptions: gitlab.ListOptions{
			PerPage: 10,
			Page:    1,
		},
	}

	for {
		ps, resp, err := git.Projects.ListProjects(opt)
		if err != nil {
			log.Errorf(err.Error())
		}

		for _, p := range ps {
			projects = append(projects, &repo.Project{
				ID:       p.ID,
				Name:     p.Name,
				Path:     p.Path,
				FullPath: p.Namespace.FullPath,
			})
		}

		if resp.CurrentPage >= resp.TotalPages {
			break
		}

		opt.Page = resp.NextPage
	}
	return projects
}

func (r *GitlabRepoBackend) ProjectsByTeam(team *repo.Team) (projects repo.Projects) {
	git, err := gitlab.NewClient(
		r.token,
		gitlab.WithBaseURL(r.url),
	)
	if err != nil {
		log.Errorf(err.Error())
	}

	opt := gitlab.ListOptions{
		PerPage: 20,
		Page:    1,
	}

	for {
		var ps []*gitlab.Project
		var resp *gitlab.Response
		var err error

		if team != nil {

			ps, resp, err = git.Groups.ListGroupProjects(team.FullPath, &gitlab.ListGroupProjectsOptions{
				Archived: gitlab.Bool(false),
				//Membership: gitlab.Bool(true),
				Search:           gitlab.String(""),
				IncludeSubgroups: gitlab.Bool(true),
				ListOptions:      opt,
			})

		} else {

			ps, resp, err = git.Projects.ListProjects(&gitlab.ListProjectsOptions{
				Archived: gitlab.Bool(false),
				//Membership: gitlab.Bool(true),
				Search:           gitlab.String(""),
				SearchNamespaces: gitlab.Bool(true),
				ListOptions:      opt,
			})

		}

		if err != nil {
			log.Errorf(err.Error())
		}

		for _, p := range ps {
			projects = append(projects, &repo.Project{
				ID:       p.ID,
				Name:     p.Name,
				Path:     p.Path,
				FullPath: p.Namespace.FullPath,
			})
		}

		if resp.CurrentPage >= resp.TotalPages {
			break
		}

		opt.Page = resp.NextPage
	}
	return projects
}
func (r *GitlabRepoBackend) ProjectBranches(id interface{}) (branches []repo.Branch) {
	git, err := gitlab.NewClient(
		r.token,
		gitlab.WithBaseURL(r.url),
	)
	if err != nil {
		log.Errorf(err.Error())
	}

	b, _, err := git.Branches.ListBranches(id, &gitlab.ListBranchesOptions{})
	if err != nil {
		log.Errorf(err.Error())
	}

	for _, item := range b {
		branches = append(branches, repo.Branch{
			Name:   item.Name,
			Merged: item.Merged,
			ID:     item.Commit.ID,
		})
	}

	return branches
}

func (r *GitlabRepoBackend) Teams() (teams repo.Teams) {
	return r.TeamsByTeam(nil)
}

func (r *GitlabRepoBackend) TeamsByTeam(team *repo.Team) (teams repo.Teams) {
	git, err := gitlab.NewClient(
		r.token,
		gitlab.WithBaseURL(r.url),
	)
	if err != nil {
		log.Errorf(err.Error())
	}

	opt := gitlab.ListOptions{
		PerPage: 20,
		Page:    1,
	}

	for {
		var g []*gitlab.Group
		var resp *gitlab.Response
		var err error

		if team != nil {
			// somehow paginate?
			g, resp, err = git.Groups.ListSubgroups(team.FullPath, &gitlab.ListSubgroupsOptions{ListOptions: opt})
		} else {
			g, resp, err = git.Groups.ListGroups(&gitlab.ListGroupsOptions{
				ListOptions: opt,
			})
		}

		if err != nil {
			log.Errorf(err.Error())
		}

		for _, item := range g {
			teams = append(teams, &repo.Team{
				Name:     item.Name,
				ID:       item.ID,
				Path:     item.Path,
				FullPath: item.FullPath,
			})
		}

		if resp.CurrentPage >= resp.TotalPages {
			break
		}

		opt.Page = resp.NextPage
	}

	return teams
}
