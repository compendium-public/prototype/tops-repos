package repo

type (
	RepoServer struct {
		Name    string      `json:"id"`
		Backend RepoBackend `json:"-"`
	}
)

// ProjectsFromTeam takes any level of team and returns all the projects beneth it
// nil for top level
func (r *RepoServer) ProjectsByTeam(team *Team) Projects {
	return r.TagProjects(r.Backend.ProjectsByTeam(team))
}

// TeamsByTeam takes any level of team and returns all the teams beneth it
// nil for top level
func (r *RepoServer) TeamsByTeam(team *Team) Teams {
	return r.TagTeams(r.Backend.TeamsByTeam(team))
}

func (r *RepoServer) Projects() Projects {
	return r.TagProjects(r.Backend.Projects())
}

func (r *RepoServer) Teams() Teams {
	return r.TagTeams(r.Backend.Teams())
}

func (r RepoServer) TagProjects(projects Projects) Projects {
	for _, item := range projects {
		item.Server = r.Name
		item.Backend = r.Backend
	}
	return projects
}

func (r RepoServer) TagTeams(teams Teams) Teams {
	for _, item := range teams {
		item.Server = r.Name
		item.Backend = r.Backend
	}
	return teams
}
