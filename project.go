package repo

type (
	Project struct {
		// Name is the git* Name for the thing, probably unique and probably key-able for the server/repo
		Name string

		ID       interface{}
		Path     string
		FullPath string
		Server   string
		Branches []Branch
		Backend  RepoBackend `json:"-"`
	}
)

func (p *Project) WithBranches() {
	p.Branches = p.GetBranches()
}

func (p *Project) GetBranches() []Branch {
	return p.Backend.ProjectBranches(p.ID)
}
