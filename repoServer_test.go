package repo

import (
	"fmt"
	"strings"
	"testing"
)

// add to mock backend for testing

var r RepoServer

func TestRepoServer(t *testing.T) {
	r = RepoServer{
		Name:    "test",
		Backend: MockBackend{},
	}
}

func TestProjects(t *testing.T) {
	ps := r.Projects()
	if len(ps) != 1 || ps[0].ID != "test" {
		out := []string{fmt.Sprintf("expecting one Project of ID test\nreceived: %s", stringify(ps))}
		for _, p := range ps {
			out = append(out, stringify(*p))
		}
		t.Fatalf(strings.Join(out, "\n  "))
	}
}

func TestTeams(t *testing.T) {
	gs := r.Teams()
	if len(gs) != 1 || gs[0].ID != "test" {
		out := []string{fmt.Sprintf("expecting one Project of ID test\nreceived: %s", stringify(gs))}
		for _, _g := range gs {
			out = append(out, stringify(*_g))
		}
		t.Fatalf(strings.Join(out, "\n  "))
	}
}
