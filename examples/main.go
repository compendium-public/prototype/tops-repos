package main

import (
	"fmt"

	repo "gitlab.com/compendium-public/prototype/tops-repos"
	"gitlab.com/compendium-public/prototype/tops-repos/backends"
)

func main() {
	repoServers := repo.RepoServers{}

	repoServers.AddServer("fake", backends.NewFakeRepoBackend())
	// repoServers.AddServer(
	// 	"gitlab-1",
	// 	NewGitlabRepoBackend("http://gitlab.devlocal.site", "token-string-here123"),
	// )

	projects, err := repoServers.Projects()
	if err != nil {
		fmt.Println(err.Error())
	}

	projects.WithBranches()

	for _, p := range projects {
		fmt.Println(p.Server, p.FullPath, p.Name, len(p.Branches))
	}

	teams, err := repoServers.Teams()
	if err != nil {
		fmt.Println(err.Error())
	}

	for _, p := range teams {
		fmt.Println(p.Server, p.FullPath, p.Name)
	}
}
