package repo

type (
	// Team is sort a generic group/sub-group/org/sub-org
	Team struct {
		// Name is the git* Nasme for the thing, probably unique and probably key-able for the server/repo
		Name string

		ID       interface{}
		FullPath string
		Path     string
		Server   string
		Backend  RepoBackend `json:"-"`
	}
)

// Teams returns the collective of sub-teams in this team
func (t Team) Teams() Teams {
	return Teams{}
}

// Projects returns the collective of Project in this team
func (t Team) Projects() Projects {
	return Projects{}
}
