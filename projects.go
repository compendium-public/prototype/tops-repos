package repo

type (
	Projects []*Project
)

func (p Projects) WithBranches() {
	for _, project := range p {
		project.WithBranches()
	}
}
