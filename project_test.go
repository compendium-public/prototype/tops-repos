package repo

import (
	"fmt"
	"testing"
)

var p Project

var mockBranches = []string{"main", "dev", "rc-1.0"}
var expectedBranchValues = stringify(mockBranches)

func TestProject(t *testing.T) {
	p = Project{
		Name:     "A Project",
		ID:       "anything",
		Path:     "123",
		FullPath: "onetwothree",
		Server:   "anywhere",
		Branches: []string{},
		Backend:  MockBackend{},
	}

}

func TestWithBranches(t *testing.T) {
	p.WithBranches()

	if stringify(p.Branches) != expectedBranchValues {
		t.Errorf("expecting: %s\n received: %s", expectedBranchValues, stringify(p.Branches))
	}
}

func TestGetBranches(t *testing.T) {
	branches := p.GetBranches()

	if stringify(branches) != expectedBranchValues {
		t.Errorf("expecting: %s\n received: %s", expectedBranchValues, stringify(branches))
	}

}

func stringify(in interface{}) string {
	return fmt.Sprintf("%v", in)
}
