package repo

import "fmt"

type MockBackend struct {
	RepoBackend
}

func (m MockBackend) ProjectBranches(id interface{}) []string {
	return mockBranches

}

func (m MockBackend) Projects() Projects {
	return []*Project{
		&Project{
			ID:      "test",
			Backend: &MockBackend{},
		},
	}
}
func (m MockBackend) Teams() Teams {
	return []*Team{
		&Team{
			ID: "test",
		},
	}
}
func (m MockBackend) ProjectsByTeam(team *Team) Projects {
	return []*Project{
		&Project{
			ID:      "test",
			Backend: &MockBackend{},
		},
	}
}
func (m MockBackend) TeamsByTeam(team *Team) Teams {
	return []*Team{
		&Team{
			ID: "test",
		},
	}
}

// TODO: move this somewhere if you like to test like this
type ATest struct {
	In      interface{}
	Out     interface{}
	Backend RepoBackend
}

func stringit(in interface{}) string {
	return fmt.Sprintf("%v", in)
}
